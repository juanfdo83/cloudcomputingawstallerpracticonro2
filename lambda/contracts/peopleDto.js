class peopleDto {
  constructor(identificacion, nombres, apellidos, estado){
    var currentDate = new Date().toISOString();
    this.identificacion = identificacion;
    this.nombres = nombres;
    this.apellidos  = apellidos;
    this.fechaRegistro  = currentDate ;
    this.estado  = estado ;
  }

  isLoad(info){
    this.identificacion = info.identificacion ? info.identificacion : "";
    this.nombres = info.nombres ? info.nombres : "";
    this.apellidos = info.apellidos ? info.apellidos : "";
    this.estado = info.estado ? info.estado : "";

    return this.identificacion != "" && this.identificacion != undefined &&
          this.nombres != "" && this.nombres != undefined &&
          this.apellidos != "" && this.apellidos != undefined &&
          this.fechaRegistro != "" && this.fechaRegistro != undefined &&
          this.estado != "" && this.estado != undefined;
  }
};

module.exports = peopleDto;