const daoManager        = require("../utilities/daoManager");
const sqsManager        = require("../utilities/sqsManager");
const util              = require("../utilities/common");
const auditoryDto       = require("../contracts/auditoryDto");
const trainingDto       = require("../contracts/trainingDto");
const trainingTable     = process.env.TABLE_TRAINING;
const colaAuditoria     = process.env.QUEUE_AUDITORY;

module.exports.handler = async (event, context) => {
   
    try {
        //identifica información inicial de entrada
        var data = JSON.parse(event.body);
        var infoRequest = new trainingDto();
        var msgId = context.awsRequestId; 

        //Valida los datos ingresados en la capacitación
        if (!infoRequest.isLoad(data)){            
            return util.cargaMensaje(400,"Solicitud Invalida!" , data); 
        }
         
        var dateNow = new Date();
        var dateProgramming = new Date(data.fechaProgramacion);

        dateNow.setHours(0,0,0,0);
        dateProgramming.setHours(0,0,0,0);
      
        if (dateNow.getTime() > dateProgramming.getTime()){
            data.estado="finalizado";
        } 
        else if (dateNow.getTime() < dateProgramming.getTime()) {
            data.estado="pospuesto";
        } 
        else if (dateNow.getTime() == dateProgramming.getTime()){
            data.estado="programado";
        }

        //envia parametros de consulta a la auditoria
        var infoAuditory = new auditoryDto(msgId, 200, "Registro Satisfactorio trainingControllerHandler");
        sqsManager.sendMessage(context,infoAuditory, colaAuditoria);
        
        // inicializa objeto para registro en bd
        var params = { TableName : trainingTable, Item: data };
        util.insertLog("Objeto para BD: " + JSON.stringify(params));
       
        // registra informacion en bd
        const response = await daoManager.register(context,params);
        util.insertLog("Result Insert BD: " + JSON.stringify(response));

        // envia parametros de resultado en bd, a la auditoría
        infoAuditory.setResponse(response);
        sqsManager.sendMessage(context,infoAuditory, colaAuditoria);

        return response;
    } catch (err) {
        util.insertLog("Error en trainingControllerHandler: " + err);
        var infoAuditory = new auditoryDto(msgId, 500, "Error en trainingControllerHandler" );  
        return util.cargaMensaje(500,"" + err);
    }     
};