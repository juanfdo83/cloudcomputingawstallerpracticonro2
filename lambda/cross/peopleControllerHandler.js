const daoManager    = require("./../utilities/daoManager");
const sqsManager    = require("./../utilities/sqsManager");
const util          = require("./../utilities/common");
const auditoryDto   = require("./../contracts/auditoryDto");
const peopleDto     = require("./../contracts/peopleDto");
const peopleTable   = process.env.TABLE_PEOPLE;
const colaAuditoria = process.env.QUEUE_AUDITORY;

module.exports.handler = async (event, context) => {
   
    try {
        // identifica información inicial de entrada
        var data = JSON.parse(event.body);
        var infoRequest = new peopleDto();
        var msgId = context.awsRequestId;   

        //Valida los datos ingresados de la persona
        if (!infoRequest.isLoad(data)){            
            return util.cargaMensaje(400,"Solicitud Invalida!" , data); 
        }
        data.fechaRegistro= new Date().toISOString();   
        // envia parametros de consulta a la auditoria
        var infoAuditory = new auditoryDto(msgId, 200, "Registro Satisfactorio peopleControllerHandler");
        sqsManager.sendMessage(context,infoAuditory, colaAuditoria);
        
        // inicializa objeto para registro en bd
        var params = { TableName : peopleTable, Item: data };
        util.insertLog("Objeto para BD: " + JSON.stringify(params));
       
        // registra informacion en bd
        const response = await daoManager.register(context,params);
        util.insertLog("Result Insert BD: " + JSON.stringify(response));

        // envia parametros de resultado en bd, a la auditoría
        infoAuditory.setResponse(response);
        sqsManager.sendMessage(context,infoAuditory, colaAuditoria);

        return response;
    } catch (err) {
        util.insertLog("Error en peopleControllerHandler: " + err);
        var infoAuditory = new auditoryDto(msgId, 500, "Error en peopleControllerHandler" );
        return util.cargaMensaje(500,"" + err);
    }     
};
