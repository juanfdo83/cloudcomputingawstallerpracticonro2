class trainingDto {
  constructor(idTraining, nombre, fechaProgramacion, nombreResponsable){
    this.idTraining = idTraining;
    this.nombre = nombre;
    this.fechaProgramacion  = fechaProgramacion;
    this.estado  = "programado" ;
    this.nombreResponsable  = nombreResponsable ;
  }

  isLoad(info){
    this.idTraining = info.idTraining ? info.idTraining : "";
    this.nombre = info.nombre ? info.nombre : "";
    this.fechaProgramacion = info.fechaProgramacion ? info.fechaProgramacion : "";
    this.nombreResponsable = info.nombreResponsable ? info.nombreResponsable : "";

    return this.idTraining != "" && this.idTraining != undefined &&
          this.nombre != "" && this.nombre != undefined &&
          this.fechaProgramacion != "" && this.fechaProgramacion != undefined &&
          this.estado != "" && this.estado != undefined &&
          this.nombreResponsable != "" && this.nombreResponsable != undefined;
  }
};

module.exports = trainingDto;