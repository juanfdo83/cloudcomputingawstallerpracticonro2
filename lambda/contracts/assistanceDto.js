class assistanceDto {
  constructor(idAssistance, identificacion, idTraining, fechaProgramacion){
    this.idAssistance = idAssistance;
    this.identificacion = identificacion;
    this.idTraining  = idTraining;
    this.fechaProgramacion  = fechaProgramacion ;
  }

  isLoad(info){
    this.idAssistance = info.idAssistance ? info.idAssistance : "";
    this.identificacion = info.identificacion ? info.identificacion : "";
    this.idTraining = info.idTraining ? info.idTraining : "";
    this.fechaProgramacion = info.fechaProgramacion ? info.fechaProgramacion : "";

    return this.idAssistance != "" && this.idAssistance != undefined &&
          this.identificacion != "" && this.identificacion != undefined &&
          this.idTraining != "" && this.idTraining != undefined &&
          this.fechaProgramacion != "" && this.fechaProgramacion != undefined;
  }
};

module.exports = assistanceDto;