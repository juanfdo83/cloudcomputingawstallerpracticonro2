const daoManager = require("./../utilities/daoManager");
const util = require("./../utilities/common")
const auditoryTable = process.env.TABLE_AUDITORY;

module.exports.handler = (event, context) => {
 
    event.Records.forEach(async(msg) => {
        try {
            var data = JSON.parse(msg.body);
            var params = { TableName : auditoryTable, Item: data };
            
            console.log(msg.body);
            console.log(data);

            util.insertLog("Objeto para BD: " + JSON.stringify(params));
                    
            const response = await daoManager.register(context,params);
            util.insertLog("Result Insert BD: " + JSON.stringify(response));
            return response;
        } catch (err) {
            util.insertLog("Error en auditoryControllerHandler: " + err);
            return util.cargaMensaje(500,"" + err);
        }     
    });
};