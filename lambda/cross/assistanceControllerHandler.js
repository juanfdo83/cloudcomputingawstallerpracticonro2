const daoManager        = require("../utilities/daoManager");
const sqsManager        = require("../utilities/sqsManager");
const util              = require("../utilities/common");
const auditoryDto       = require("../contracts/auditoryDto");
const assistanceDto     = require("../contracts/assistanceDto");
const assistanceTable   = process.env.TABLE_ASSISTANCE;
const colaAuditoria     = process.env.QUEUE_AUDITORY;
const tablePeople       = process.env.TABLE_PEOPLE;
const tableTraining     = process.env.TABLE_TRAINING;

module.exports.handler = async (event, context) => {

    try {
        // identifica información inicial de entrada
        var data = JSON.parse(event.body);         
        var infoRequest = new assistanceDto();
        var msgId = context.awsRequestId; 
        
        //Valida los datos ingresados en la asistencia
        if (!infoRequest.isLoad(data)){            
            return util.cargaMensaje(400,"Solicitud Invalida!" , data); 
        }
          
        //Busqueda Capacitación
        var statusPeople= await daoManager.searchById(context,tablePeople,'identificacion',data.identificacion);
        util.insertLog("Resultdo Consulta Status People DB: " +  JSON.stringify(statusPeople));
        if (!statusPeople){ 
            util.insertLog("202-Solicitud rechazada: La persona se encuentra inactiva." + data);              
            return util.cargaMensaje(202,"Solicitud rechazada: La persona se encuentra inactiva." , data);     
        }
        else{
                var statusTraining= await daoManager.searchById(context,tableTraining,'idTraining',data.idTraining);
                util.insertLog("Resultdo Consulta Status Training DB: " +  JSON.stringify(statusTraining));
                if (!statusTraining){
                    util.insertLog("202-Solicitud rechazada: El curso se encuentra inactiva." + data); 
                    return util.cargaMensaje(202,"Solicitud rechazada: El curso se encuentra inactiva." , data);  
                }else{

                // envia parametros de consulta a la auditoria
                var infoAuditory = new auditoryDto(msgId, 200, "Registro Satisfactorio assistanceControllerHandler");
  
                //Registro de asistencia
                var infoAssintance = new assistanceDto(data.idAssistance,data.identificacion,data.idTraining,data.fechaProgramacion);
                var params ={TableName: assistanceTable, Item: infoAssintance};
                var response = await daoManager.register(context,params);
                util.insertLog("Objeto para Asistencia DB: " +  JSON.stringify(params));

                sqsManager.sendMessage(context,infoAuditory, colaAuditoria);        
                
                // inicializa objeto para registro en bd
                var params = { TableName : assistanceTable, Item: data };
                util.insertLog("Objeto para BD: " + JSON.stringify(params));
               
                // registra informacion en bd
                response = await daoManager.register(context,params);
                util.insertLog("Result Insert BD: " + JSON.stringify(response));
        
                // envia parametros de resultado en bd, a la auditoría
                infoAuditory.setResponse(response);
                sqsManager.sendMessage(context,infoAuditory, colaAuditoria);        
                return response;
            }
        }        
    } catch (err) {
        util.insertLog("Error en assistanceControllerHandler: " + err);
        var infoAuditory = new auditoryDto(msgId, 500, "Error en assistanceControllerHandler" );
        return util.cargaMensaje(500,"" + err);
    }     
};