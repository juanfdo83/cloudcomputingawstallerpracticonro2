class auditoryDto {
  constructor(eventId, state, description){   
    var currentDate = new Date().toISOString();
    this.id = eventId;   
    this.registerDate = currentDate;
    this.state = state;
    this.description = description; 
  }

  setResponse(resp){
    var currentDate = new Date().toISOString();
    this.response=resp;
    this.responseDate=currentDate;
  }
};

module.exports = auditoryDto;