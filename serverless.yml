service: ${self:custom.projectFullName}
frameworkVersion: '2'
useDotenv: true

provider:
  name: aws
  runtime: nodejs12.x
  lambdaHashingVersion: 20201221
  profile: serverlessUser
  stackTags:
    Ambiente: ${self:custom.stage}
    AreaResponsable: DevelopStefanini
    Comentarios: " "
    Criticidad: ALTA
    Disponibilidad: 7x24
    Nube: AWS
    Plataforma: Serverless Framework
    Proyecto: ${self:custom.project}
    Tecnologia: Serverless
    UnidadNegocio: Fábrica de Sofware
  region: us-east-2

plugins:
  - serverless-webpack

custom:
  projectFullName: ${self:custom.project}
  stage: dev
  project: AWSTrainingApplication
  integrationBaseStackName: awstraining-api-gateway-${self:custom.stage}
  tableAuditory: ddb-${self:custom.projectFullName}-${self:custom.stage}-AuditoryTable
  queueAuditory: sqs-${self:custom.projectFullName}-${self:custom.stage}-AuditoryQueue
  queuePeople: sqs-${self:custom.projectFullName}-${self:custom.stage}-PeopleQueue
  basePath: ${self:custom.projectFullName}/${self:custom.stage}
  tablePeople: ddb-${self:custom.projectFullName}-${self:custom.stage}-PeopleTable
  tableTraining: ddb-${self:custom.projectFullName}-${self:custom.stage}-TrainingTable
  tableAssistance: ddb-${self:custom.projectFullName}-${self:custom.stage}-AssistanceTable

package:
  individually: true

functions:
  auditory:
    name: func-${self:custom.projectFullName}-${self:custom.stage}-auditory
    handler: lambda/cross/auditoryControllerHandler.handler
    environment:
      TABLE_AUDITORY:      ${self:custom.tableAuditory}     
    role: auditoryLambdaRole
    events:
        - sqs:
            arn:
              Fn::GetAtt:
                - RestrictiveListsAuditoryQueue
                - Arn
  people:
    name: func-${self:custom.projectFullName}-${self:custom.stage}-people
    handler: lambda/cross/peopleControllerHandler.handler
    role: peopleLambdaRole
    environment:
      QUEUE_AUDITORY:       ${self:custom.queueAuditory}
      TABLE_PEOPLE:         ${self:custom.tablePeople}
    events:
        - http:
            path: ${self:custom.basePath}/testPeople
            method: POST
            cors: true
  training:
    name: func-${self:custom.projectFullName}-${self:custom.stage}-training
    handler: lambda/cross/trainingControllerHandler.handler
    role: trainingLambdaRole
    environment:
      QUEUE_AUDITORY:         ${self:custom.queueAuditory}
      TABLE_TRAINING:         ${self:custom.tableTraining}
    events:
        - http:
            path: ${self:custom.basePath}/testTraining
            method: POST
            cors: true
  assistance:
    name: func-${self:custom.projectFullName}-${self:custom.stage}-assistance
    handler: lambda/cross/assistanceControllerHandler.handler
    role: assistanceLambdaRole
    environment:
      QUEUE_AUDITORY:           ${self:custom.queueAuditory}
      TABLE_ASSISTANCE:         ${self:custom.tableAssistance}
      TABLE_PEOPLE:         ${self:custom.tablePeople}
      TABLE_TRAINING:         ${self:custom.tableTraining}
    events:
        - http:
            path: ${self:custom.basePath}/testAssistance
            method: POST
            cors: true
resources:
  Resources:
    peopleLambdaRole: 
      Type: AWS::IAM::Role
      Properties: 
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Principal:
                Service:
                - lambda.amazonaws.com
              Action:
                - 'sts:AssumeRole'
        Description: 'Lambda Role'
        ManagedPolicyArns: 
          - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
          - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        Path: /
        Policies:
          - PolicyName: 'send-message-sqs-auditory-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - sqs:SendMessage        
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryQueue, Arn ]
          - PolicyName: 'write-dynamodb-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - dynamodb:PutItem
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsPeopleTable, Arn ]
        RoleName: iam-${self:custom.projectFullName}-${self:custom.stage}-peopleLambdaRole
        Tags: 
          - Key: Name
            Value: iam-${self:custom.projectFullName}-${self:custom.stage}-peopleLambdaRole
    assistanceLambdaRole: 
      Type: AWS::IAM::Role
      Properties: 
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Principal:
                Service:
                - lambda.amazonaws.com
              Action:
                - 'sts:AssumeRole'
        Description: 'Lambda Role'
        ManagedPolicyArns: 
          - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
          - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        Path: /
        Policies:
          - PolicyName: 'send-message-sqs-auditory-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - sqs:SendMessage        
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryQueue, Arn ]
          - PolicyName: 'write-dynamodb-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - dynamodb:PutItem
                    - dynamodb:GetRecords
                    - dynamodb:GetItem
                    - dynamodb:Scan
                    - dynamodb:Query
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsPeopleTable, Arn ]
                    - Fn::GetAtt: [ RestrictiveListsTrainingTable, Arn ]
                    - Fn::GetAtt: [ RestrictiveListsAssistanceTable, Arn ]
                    - Fn::GetAtt: [ RestrictiveListsAuditoryTable, Arn ]
        RoleName: iam-${self:custom.projectFullName}-${self:custom.stage}-assistanceLambdaRole
        Tags: 
          - Key: Name
            Value: iam-${self:custom.projectFullName}-${self:custom.stage}-assistanceLambdaRole
    trainingLambdaRole: 
      Type: AWS::IAM::Role
      Properties: 
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Principal:
                Service:
                - lambda.amazonaws.com
              Action:
                - 'sts:AssumeRole'
        Description: 'Lambda Role'
        ManagedPolicyArns: 
          - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
          - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        Path: /
        Policies:
          - PolicyName: 'send-message-sqs-auditory-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - sqs:SendMessage        
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryQueue, Arn ]
          - PolicyName: 'write-dynamodb-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - dynamodb:PutItem
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsTrainingTable, Arn ]
        RoleName: iam-${self:custom.projectFullName}-${self:custom.stage}-trainingLambdaRole
        Tags: 
          - Key: Name
            Value: iam-${self:custom.projectFullName}-${self:custom.stage}-trainingLambdaRole
    auditoryLambdaRole: 
      Type: AWS::IAM::Role
      Properties: 
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Principal:
                Service:
                - lambda.amazonaws.com
              Action:
                - 'sts:AssumeRole'
        Description: 'Lambda Role'
        ManagedPolicyArns: 
          - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
          - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        Path: /
        Policies:
          - PolicyName: 'read-message-sqs-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - sqs:DeleteMessage
                    - sqs:ReceiveMessage
                    - sqs:GetQueueAttributes
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryQueue, Arn ]
          - PolicyName: 'write-dynamodb-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - dynamodb:PutItem
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryTable, Arn ]
        RoleName: iam-${self:custom.projectFullName}-${self:custom.stage}-auditoryLambdaRole
        Tags: 
          - Key: Name
            Value: iam-${self:custom.projectFullName}-${self:custom.stage}-auditoryLambdaRole
    testAuditoryLambdaRole: 
      Type: AWS::IAM::Role
      Properties: 
        AssumeRolePolicyDocument:
          Version: '2012-10-17'
          Statement:
            - Effect: Allow
              Principal:
                Service:
                - lambda.amazonaws.com
              Action:
                - 'sts:AssumeRole'
        Description: 'Lambda Role'
        ManagedPolicyArns: 
          - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
          - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        Path: /
        Policies:
          - PolicyName: 'send-message-sqs-policy'
            PolicyDocument:
              Version: '2012-10-17'
              Statement:
                - Effect: Allow
                  Action:
                    - sqs:SendMessage
                  Resource:
                    - Fn::GetAtt: [ RestrictiveListsAuditoryQueue, Arn ]
        RoleName: iam-${self:custom.projectFullName}-${self:custom.stage}-testAuditoryLambdaRole
        Tags: 
          - Key: Name
            Value: iam-${self:custom.projectFullName}-${self:custom.stage}-testAuditoryLambdaRole
    RestrictiveListsAuditoryTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.tableAuditory}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        BillingMode: PAY_PER_REQUEST
    RestrictiveListsAuditoryQueue:
          Type: AWS::SQS::Queue
          Properties:
            QueueName: ${self:custom.queueAuditory}
    RestrictiveListsPeopleQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: ${self:custom.queuePeople}
    RestrictiveListsPeopleTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.tablePeople}
        AttributeDefinitions:
          - AttributeName: identificacion
            AttributeType: S
        KeySchema:
          - AttributeName: identificacion
            KeyType: HASH            
        BillingMode: PAY_PER_REQUEST        
    RestrictiveListsTrainingTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.tableTraining}
        AttributeDefinitions:
          - AttributeName: idTraining
            AttributeType: N
        KeySchema:
          - AttributeName: idTraining
            KeyType: HASH            
        BillingMode: PAY_PER_REQUEST
    RestrictiveListsAssistanceTable: 
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:custom.tableAssistance}
        AttributeDefinitions:
          - AttributeName: idAssistance
            AttributeType: N
        KeySchema:
          - AttributeName: idAssistance
            KeyType: HASH            
        BillingMode: PAY_PER_REQUEST     