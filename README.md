# AWS.Training.Application
contratos de prueba para cada API:
testPeople:
{
    "identificacion"  : "71367063",
    "nombres"  : "Juan Fernando",
    "apellidos"  : "Vásquez Correa",
    "estado"  : "activo"
}

testTraining
{
    "idTraining" : 4,
    "nombre"  : "Quimica",
    "fechaProgramacion"  : "2021-09-27",
    "nombreResponsable" : "Daniel Perez"
}

testAssistance
{
    "idAssistance" : 7,
    "identificacion"  : "43435098",
    "idTraining"  : 1,
    "fechaProgramacion"  : "2021-08-19"
}
